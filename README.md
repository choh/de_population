# Visualisation of Germany's Popolation
This repository contains a small data visualisation project making 

* a waffle plot of the German population by muncipality size
* a plot of the female population by muncipality size

To generate the plots just run the files in the `R` directory in order, given
you have the raw data in a directory named `data-raw` and all dependencies 
installed.

## Data Source
The data to create the visualisations can be obtained from [Destatis](https://www.destatis.de/DE/Themen/Laender-Regionen/Regionales/Gemeindeverzeichnis/_inhalt.html).

You only need the current GV100AD file and the [gv100 R package](https://gitlab.com/choh/gv100) to read it into R. 
The source in this repo expects this data in a `data-raw` directory.

## Dependencies
You need the following R packages to run the code: `dplyr`, `ggplot2`, `gv100`, `here`, `hrbrthemes`, `stringr`, `tidyr`, `waffle`. Furthermore the plots 
require the IBM Plex Sans and Source Sans Pro Fonts.
